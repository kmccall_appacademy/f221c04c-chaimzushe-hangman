class Hangman
  attr_reader :guesser, :referee, :board
  MAX_GUESSES  = 8
  def initialize(players = {:guesser => player1, :referee => player2})
    @guesser = players[:guesser]
    @referee = players[:referee]
    @num_remaining_guesses = MAX_GUESSES

  end

   def setup
      secret_length = referee.pick_secret_word
      guesser.register_secret_length(secret_length)
      @board = [nil] * secret_length
   end
   #
   def take_turn
     letter = guesser.guess(@board)
     matching_indicese = referee.check_guess(letter)
     update_board(letter, matching_indicese)
     @num_remaining_guesses -= 1 if matching_indicese.empty?
     display_board
     guesser.handle_response(letter, matching_indicese)
   end

   def display_board
      display = ""
      board.each do |el|
        el.nil? ? display << "_" :  display << el
      end
      puts display
   end

   def update_board(guess, matching_indicese)
      matching_indicese.each{|inx| board[inx] = guess}
   end

   def won?
      board.join == referee.secret_word
   end

end

class HumanPlayer

  attr_reader :secret_word_length, :secret_word, :secret_word_length


  def register_secret_length(length)
     puts "the length of the secret word is: #{length}"
     @secret_word_length = length

  end

  def guess(board)
   print "please enter your letter to guess: "
   letter = gets.chomp
  end

  def pick_secret_word
    puts "what is the secret word"
    @secret_word = gets.chomp
    @secret_word.length
  end

  def check_guess(char)
    matching_indices = []
    @secret_word.chars.each_with_index do |secret_char, index|
      matching_indices << index if secret_char == char
    end
    matching_indices
  end

  def handle_response(letter, matching_indicese)

  end


end


class ComputerPlayer
  attr_reader :dictionary, :secret_word, :secret_word_length, :candidate_words
  attr_accessor :guesses

  def self.player_with_dict_file(dict_file)
     ComputerPlayer.new(File.readsines(dict_file).map(&:chomp))
  end

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = dictionary.sample
    @secret_word.length
  end

  def register_secret_length(length)
     puts "the length of the secret word is: #{length}"
      @candidate_words = @dictionary.select{ |words| words.size == length}

  end

  def guess(board)
    guessed_letters = board.select{ |ele| !ele.nil? }.join("")
    all_chars = @candidate_words.join
    uniq_chars = all_chars.chars.select{ |char| !guessed_letters.include?(char) }.uniq
    letter = uniq_chars.reduce(uniq_chars[0]) do |initial_value, ele|
      if all_chars.count(ele) > all_chars.count(initial_value)
         ele
      else
         initial_value
      end
    end
    letter
  end

  def collapse_word(word, indices) # 0 rear
    str = ""
    indices.each do  |indice|
       str += word[indice]
    end
    str
  end


  def handle_response(letter, matching_indices)
    compare_to = letter * matching_indices.size
    @candidate_words.select! do |word|
      collapse_word(word,matching_indices)  == compare_to &&
      compare_to.length == word.count(letter)
    end


  end

  def check_guess(char)
    matching_indices = []
    @secret_word.chars.each_with_index do |secret_char, index|
      matching_indices << index if secret_char == char
    end
    matching_indices
  end
end
